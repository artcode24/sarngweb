/** @type {import('next').NextConfig} */

const webpack = require("webpack")

const nextConfig = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  experimental: {
    externalDir:
      true |
      {
        enabled: true,
        silent: true,
      },
  },
  webpack: (config, { dev }) => {
    config.plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
      })
    )
    return config
  },
}

module.exports = nextConfig

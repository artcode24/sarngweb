import React, { useState, useEffect } from "react"

const Herder = () => {
  useEffect(() => {}, [])

  return (
    <header className="sarngweb-header">
      <img className="sarngweb-nav-logo logo" src="/sarnweb-logo-3.png" />
      <nav>
        <ul className="nav-links">
          <li>
            <a href="#">บริการของเรา</a>
          </li>
          <li>
            <a href="#">ผลงาน</a>
          </li>
          <li>
            <a href="#">จ้างงาน</a>
          </li>
          <li>
            <a href="#">แพ็กเกจ</a>
          </li>
          <li>
            <a href="#">
              เราคือใคร{" "}
              <font color="#FFB703">
                <b>?</b>
              </font>
            </a>
          </li>
          <li>
            <a href="#">ติดต่อเรา</a>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Herder

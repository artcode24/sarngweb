import { Fragment } from "react"
import MainFooter from "./footer"
import MainHeader from "./header"
import { Layout } from "antd"
const { Header, Content, Footer } = Layout

const MainLayout = ({ children }) => {
  return (
    <Layout>
      <MainHeader />
      <Content
        className="site-layout"
        style={{ padding: "0px 7%", marginTop: 64 }}
      >
        <main>{children}</main>
      </Content>
      <Footer>
        <MainFooter />
      </Footer>
    </Layout>
  )
}

export default MainLayout

import "../css/global.css"
import "../css/component.css"
import "../css/navbar.css"
import "../css/custom.css"

const MyApp = ({ Component, pageProps }) => {
  return <Component {...pageProps} />
}

export default MyApp
